package com.example.almirantonio.noticicacao;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ScreenCalledByNotificationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_called_by_notification);
    }
}
